<%--
  Created by IntelliJ IDEA.
  User: deedo
  Date: 11/27/2020
  Time: 11:04 PM
  To change this template use File | Settings | File Templates.
--%>

<%--Index file opens when the Tomcat Server is ran.
Allows user to enter the information for a new family member.--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Wallace Family Member Application</title>
</head>

<body>
    <div style="text-align: center;">
        <h1>The Wallace Family</h1>
        <h3>Enter the information for the new family member.</h3>
    </div>
    <div align="center">
        <form action="MemberServlet" method="post">
            <table border="1" cellpadding="5">
                <tr><th>Name:</th><td><input name="name" type="text"></td></tr>
                <tr><th>Preferred Name:</th><td><input name="pname" type="text"></td></tr>
                <tr><th>Address Line 1:</th><td><input name="addr1" type="text"></td></tr>
                <tr><th>Address Line 2:</th><td><input name="addr2" type="text"></td></tr>
                <tr><th>Address Line 3:</th><td><input name="addr3" type="text"></td></tr>
                <tr><th>Phone:</th><td><input name="phone" type="tel"></td></tr>
                <tr><th>Birthday:</th><td><input name="bday" type="date"></td></tr>
                <tr><th>Birthday Month:</th><td><input name="month" type="text"></td></tr>
                <tr><th>Anniversary:</th><td><input name="anniversary" type="date"></td></tr>
                <tr><td colspan="2" align="center"><input type="submit" value="Save" /></td></tr>
            </table>
        </form>
    </div>
</body>
</html>

package com.jdwindland;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class MemberDAO {

    //Method to access database and return list of family members.
    public static void listMembers(){
        Transaction t = null;
        try (Session session = HibernateUtil.getFactory().openSession()) {
            t = session.beginTransaction();
            //COLLECTION - List
            List fmember = session.createQuery("From Member").list();
            System.out.println("\n-----------------------------------------------------------------------------------" +
                    "------------------------------------------------------------------------------------");
            System.out.printf("%-5s %-20s %-10s %-30s %-30s %-30s %-13s %-12s %-12s %-12s \n","ID", "Name",
                    "Preferred", "Address 1", "Address 2", "Address 3", "Phone", "Birthday", "Month",
                    "Anniversary");
            System.out.println("-----------------------------------------------------------------------------------" +
                    "------------------------------------------------------------------------------------");
            for (Object o : fmember) {
                Member member = (Member) o;
                System.out.printf("%-5s %-20s %-10s %-30s %-30s %-30s %-13s %-12s %-12s %-12s \n", member.getId(),
                        member.getName(), member.getPname(), member.getAddr1(), member.getAddr2(), member.getAddr3(),
                        member.getPhone(), member.getBday(), member.getBdaymo(), member.getAnniversary());
            }
            t.commit();
        } catch (HibernateException e) {
            if (t != null) t.rollback();
            System.out.print("\nUnable to list family members.\n");
            e.printStackTrace();
        }
    }

    //Method to delete a family member from database.
    public static void deleteMember(Integer id){
        Transaction t;
        try (Session session = HibernateUtil.getFactory().openSession()) {
            t = session.beginTransaction();
            Member member = session.get(Member.class, id);
            session.delete(member);
            t.commit();
        } catch (HibernateException e) {
            System.out.println("\nUnable to delete the pet.\n");
            e.printStackTrace();
        }
    }

    //method to update a specific family member in the database.
    public static void updateMember(Integer id, String name, String pname, String addr1, String addr2, String addr3, String phone, String bday, String month, String anniversary){
        Transaction t;
        try (Session session = HibernateUtil.getFactory().openSession()) {
            t = session.beginTransaction();
            Member member = session.get(Member.class, id);
            member.setName(name);
            member.setPname(pname);
            member.setAddr1(addr1);
            member.setAddr2(addr2);
            member.setAddr3(addr3);
            member.setPhone(phone);
            member.setBday(bday);
            member.setBdaymo(month);
            member.setAnniversary(anniversary);
            session.update(member);
            t.commit();
        } catch (HibernateException e) {
            System.out.println("\nUnable to update the family member.\n");
            e.printStackTrace();
        }
    }



}

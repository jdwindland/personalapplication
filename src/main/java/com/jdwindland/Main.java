package com.jdwindland;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

//Main class that runs program.
// Lists all the family members and gives user option to delete or update a family member.
public class Main {

    public static void main(String[] args) throws IOException {

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        boolean valid = false;
        boolean pass = false;

        MemberDAO.listMembers();

	    /*Asks user to select option. The option selected will determine if user
        updates a family member, delete a family member or do nothing.*/
            while(!pass) {
                try {
                    System.out.println("\nEnter an option: \n"
                            + "1) Update a family member \n"
                            + "2) Delete a family member \n"
                            + "3) Close Program \n");

                    int option = Integer.parseInt(reader.readLine());

                    if (option == 1) {
                        valid = true;
                        pass = true;
                    }
                    if (option == 2) {
                        valid = true;
                        pass = true;
                    }
                    if (option == 3) {
                        valid = true;
                        pass = true;
                    }
                    if (valid) {
                        switch (option) {
                            /*Asks user to enter current information of family member and calls method
                            to update information in database.*/
                            case 1 -> {
                                Scanner systemInScanner = new Scanner(System.in);
                                System.out.print("Enter the name: ");
                                String name = systemInScanner.nextLine();
                                System.out.print("Enter the preferred name: ");
                                String pname = systemInScanner.nextLine();
                                System.out.print("Enter address line 1: ");
                                String addr1 = systemInScanner.nextLine();
                                System.out.print("Enter address line 2: ");
                                String addr2 = systemInScanner.nextLine();
                                System.out.print("Enter address line 3: ");
                                String addr3 = systemInScanner.nextLine();
                                System.out.print("Enter the phone number: ");
                                String phone = systemInScanner.nextLine();
                                System.out.print("Enter the birthday: ");
                                String bday = systemInScanner.nextLine();
                                System.out.print("Enter the birthday month: ");
                                String month = systemInScanner.nextLine();
                                System.out.print("Enter the anniversary: ");
                                String anniversary = systemInScanner.nextLine();
                                System.out.print("Enter the id of the person you want to update: ");
                                int id = systemInScanner.nextInt();
                                MemberDAO.updateMember(id, name, pname, addr1, addr2, addr3, phone, bday, month, anniversary);
                                MemberDAO.listMembers();
                            }
                            //Calls the method to delete family member and then prints a new list.
                            case 2 -> {
                                Scanner systemInScanner = new Scanner(System.in);
                                System.out.print("Enter the id of the person you want to delete: ");
                                int id = systemInScanner.nextInt();
                                MemberDAO.deleteMember(id);
                                MemberDAO.listMembers();
                            }
                            //Does nothing put allow program to continue and end.
                            case 3 -> {
                            }
                        }
                    } else {
                        System.out.println("\nPlease try again and enter a valid option.\n");
                    }
                } catch (NumberFormatException e) {
                    System.out.println("\nPlease try again and enter a valid option.\n");
                }catch (InputMismatchException e) {
                    System.out.println("\nPlease try again and enter a valid family member id.\n");
                }catch (NoSuchElementException | IllegalStateException e) {
                    System.out.println("\nYour input was invalid.\n");
                }catch (NullPointerException | IllegalArgumentException e) {
                    System.out.println("\nYou did not enter a valid id.\n");
                }
            }
    }
}

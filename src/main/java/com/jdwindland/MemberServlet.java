package com.jdwindland;

import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//Servlet class to save the information entered on the JSP page and save it ot the database.
//A message is posted back to the JSP page confirming what was entered.
@WebServlet(name = "MemberServlet", urlPatterns = {"/MemberServlet"})
public class MemberServlet extends HttpServlet {
    private static final long serialVersionUID = 1;

    public void init(){
        MemberDAO memberDAO = new MemberDAO();
    }

    //Post Method
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws org.hibernate.HibernateException {
        response.setContentType("text/html");

        try{
            //Capture the information entered on JSP page.
            PrintWriter writer = response.getWriter();
            String name = request.getParameter("name");
            String pname = request.getParameter("pname");
            String addr1 = request.getParameter("addr1");
            String addr2 = request.getParameter("addr2");
            String addr3 = request.getParameter("addr3");
            String phone = request.getParameter("phone");
            String bday = request.getParameter("bday");
            String month = request.getParameter("month");
            String anniversary = request.getParameter("anniversary");

            //Saves the information using Hibernate into the database.
            Session session = HibernateUtil.getFactory().openSession();
            Transaction transaction = session.beginTransaction();
            Member member = new Member(name, pname, addr1, addr2, addr3, phone, bday, month, anniversary);
            session.save(member);
            transaction.commit();
            session.close();

            //Post the information back to JSP page.
            writer.println("<html><head></head>");
            writer.println("<body>");
            writer.println("<div style=\"text-align: center;\">");
            writer.println("<h3>" + name + " was added to the list.</h3>");
            writer.println("</div><div align=\"center\"><table border=\"1\" cellpadding=\"5\">");
            writer.println("<tr><th>Name:</th><td> " + name + "</td></tr>");
            writer.println("<tr><th>Preferred Name:</th><td> " + pname + "</td></tr>");
            writer.println("<tr><th>Address Line 1:</th><td> " + addr1 + "</td></tr>");
            writer.println("<tr><th>Address Line 2:</th><td> " + addr2 + "</td></tr>");
            writer.println("<tr><th>Address Line 3:</th><td> " + addr3 + "</td></tr>");
            writer.println("<tr><th>Phone:</th><td> " + phone + "</td></tr>");
            writer.println("<tr><th>Birthday:</th><td> " + bday + "</td></tr>");
            writer.println("<tr><th>Birthday Month:</th><td> " + month + "</td></tr>");
            writer.println("<tr><th>Anniversary:</th><td> " + anniversary + "</td></tr>");
            writer.println("</table>");
            writer.println("<h3><a href=\"http://localhost:8080/PersonalApplication_war_exploded/index.jsp\">Add Another Family Member</a></h3>");
            writer.println("</div></body></html>");
        }catch(IOException e){
            System.out.println("There was a failure with the Servlet.");
        }catch(org.hibernate.HibernateException e){
            System.out.println("There was a failure with Hibernate.");
        }
    }

    //Get Method
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter writer = response.getWriter();
            response.setContentType("text/html");
            writer.println("Get Method");
        }catch(IOException e){
            System.out.println("There was a failure with the Servlet.");
        }
    }
}

package com.jdwindland;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

//Hibernate Util class in instantiate the Session Factory.
public class HibernateUtil {

    private static SessionFactory sessionFactory = null;

    public static SessionFactory getFactory() throws org.hibernate.HibernateException {
        if (sessionFactory == null) {
            try {
                sessionFactory = new Configuration().configure().buildSessionFactory();
            } catch (org.hibernate.HibernateException e) {
                System.out.println("There was an issue building the Session Factory.");
            }
        }
        return sessionFactory;
    }
}
